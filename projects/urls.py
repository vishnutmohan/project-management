from django.conf.urls import url

from .views import *
from .viewsets import *

urlpatterns = [
    url(r'project-list/$', project_list, name='project-list'),
    url(r'project-add/$', project_add, name='project-add'),
    url(r'(?P<pk>\d+)/project-view/$', project_view, name='project-view'),
    url(r'(?P<pk>\d+)/project-edit/$', project_edit, name='project-edit'),
    url(r'(?P<pk>\d+)/project-delete/$', project_delete, name='project-delete'),
    url(r'get-all-projects/$', ProjectViewSet.as_view({
        'get': 'list_all_project',
    })),
    url(r'get-project/(?P<pk>\d+)/$', ProjectViewSet.as_view({
        'get': 'list_single_project'
    })),
]
