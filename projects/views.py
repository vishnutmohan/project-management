from django.contrib import messages
from django.shortcuts import render, redirect

from decorators import *
from .models import Project
from .forms import *
from employees.models import Employee


# Create your views here.
@loggedin_or_error
@employee_or_error
def project_list(request):
    projects = Project.objects.all()
    return render(request, 'project_list.html', {
        'projects': projects
    })


@loggedin_or_error
@employee_or_error
def project_view(request, pk=None):
    project = Project.objects.get(id=pk)
    return render(request, 'project_view.html', {
        'project': project,
    })


@loggedin_or_error
@employee_or_error
def project_add(request):
    form = AddProjectForm(request.POST or None)
    employees = Employee.objects.all()
    users = User.objects.all()
    if form.is_valid():
        form.save()
        messages.error(request, 'Project added successfully')
        return redirect('project-list')
    return render(request, 'project_add.html', {
        'form': form,
        'employees': employees,
        'users': users,
    })


@loggedin_or_error
@manager_or_error
@employee_or_error
def project_edit(request, pk):
    form = EditProjectForm(request.POST or None)
    project = Project.objects.get(id=pk)
    project_employees = project.members.all()
    other_employees = Employee.objects.exclude(id__in=project_employees)
    if form.is_valid():
        project.name = form.cleaned_data.get('name')
        project.description = form.cleaned_data.get('description')
        project.members = form.cleaned_data.get('members')
        project.manager = form.cleaned_data.get('manager')
        project.save()
        messages.error(request, 'Project updated successfully')
        return redirect('project-list')
    return render(request, 'project_edit.html', {
        'form': form,
        'project': project,
        'project_employees': project_employees,
        'other_employees': other_employees,
    })


@loggedin_or_error
@manager_or_error
@employee_or_error
def project_delete(request, pk):
    project = Project.objects.get(id=pk)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Yes':
            project.delete()
            messages.error(request, "Project deletion Success.")
            return redirect('project-list')
        messages.error(request, "Project deletion failed.")
        return redirect('project-list')
    msg = "Do you want to delete project %s" % project
    return render(request, 'delete_confirmation.html', {
        'message': msg,
    })
