from rest_framework import serializers

from .models import *
from employees.serializers import *


class ProjectSerializer(serializers.ModelSerializer):
    members = serializers.StringRelatedField(many=True)
    manager = serializers.StringRelatedField()

    class Meta:
        model = Project
        fields = [
            'id',
            'name',
            'description',
            'manager',
            'members',
        ]

