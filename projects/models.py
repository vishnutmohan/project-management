from __future__ import unicode_literals

from employees.models import *

from django.db import models


# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    manager = models.ForeignKey(Employee, related_name='project_manager', null=True, blank=True)
    members = models.ManyToManyField(Employee, related_name='project_members')

    def __unicode__(self):
        return self.name
