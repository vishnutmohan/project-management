from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import *


class ProjectViewSet(viewsets.ViewSet):
    def list_all_project(self, request, *args, **kwargs):
        dept_queryset = Project.objects.all()
        serializer = ProjectSerializer(dept_queryset, many=True)
        return Response(serializer.data)

    def list_single_project(self, request, pk, *args, **kwargs):
        project = get_object_or_404(Project, id=pk)
        serializer = ProjectSerializer(project)
        return Response(serializer.data)
