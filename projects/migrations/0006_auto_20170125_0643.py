# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2017-01-25 06:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0005_auto_20170125_0642'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='project_manager',
            new_name='manager',
        ),
    ]
