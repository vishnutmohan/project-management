from django import forms

from .models import *


class AddProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            'name',
            'description',
            'manager',
            'members'
        ]


class EditProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            'name',
            'description',
            'manager',
            'members'
        ]
