from django import forms

from .models import *


class AddDeptForm(forms.ModelForm):
    class Meta:
        model = Department

        fields = [
            'name'
        ]
