from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import *


class DepartmentViewSet(viewsets.ViewSet):
    def list_all_department(self, request, *args, **kwargs):
        dept_queryset = Department.objects.all()
        serializer = DepartmentSerializer(dept_queryset, many=True)
        return Response(serializer.data)

    def list_single_department(self, request, pk, *args, **kwargs):
        department = get_object_or_404(Department, id=pk)
        serializer = DepartmentSerializer(department)
        return Response(serializer.data)
