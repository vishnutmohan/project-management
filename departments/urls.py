from django.conf.urls import url

from .views import *
from .viewsets import *

urlpatterns = [
    url(r'dept-list/$', dept_list, name='dept-list'),
    url(r'(?P<pk>\d+)/dept-view/$', dept_view, name='dept-view'),
    url(r'dept-add/$', dept_add, name='dept-add'),
    url(r'(?P<pk>\d+)/dept-edit/$', dept_edit, name='dept-edit'),
    url(r'(?P<pk>\d+)/dept-delete/$', dept_delete, name='dept-delete'),
    url(r'get-all-departments/$', DepartmentViewSet.as_view({
        'get': 'list_all_department',
    })),
    url(r'get-department/(?P<pk>\d+)/$', DepartmentViewSet.as_view({
        'get': 'list_single_department'
    })),
]
