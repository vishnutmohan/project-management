from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect

from decorators import loggedin_or_error, employee_or_error
from .models import *
from employees.models import Employee
from .forms import *


# Create your views here.

@loggedin_or_error
@employee_or_error
def dept_list(request):
    departments = Department.objects.all()
    return render(request, 'dep_list.html', {
        'departments': departments
    })


@loggedin_or_error
@employee_or_error
def dept_view(request, pk=None):
    department = Department.objects.get(id=pk)
    employees = Employee.objects.filter(department=department)
    return render(request, 'dept_view.html', {
        'department': department,
        'employees': employees
    })


@loggedin_or_error
@employee_or_error
def dept_add(request):
    form = AddDeptForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.error(request, ' New department added successfully')
        return redirect('dept-list')
    return render(request, 'dept_add.html', {
        'form': form
    })


@loggedin_or_error
@employee_or_error
def dept_edit(request, pk=None):
    department = Department.objects.get(id=pk)
    form = AddDeptForm(request.POST or None)
    if form.is_valid():
        department.name = form.cleaned_data.get('name')
        department.save()
        messages.error(request, 'Department updated successfully')
        return redirect('dept-list')
    return render(request, 'dept_edit.html', {
        'department': department,
        'form': form
    })


@loggedin_or_error
@employee_or_error
def dept_delete(request, pk=None):
    department = Department.objects.get(id=pk)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Yes':
            Employee.objects.filter(department=department).update(department="")
            department.delete()
            messages.error(request, "Department deletion Success.")
            return redirect('dept-list')
        messages.error(request, "User deletion failed.")
        return redirect('dept-list')
    msg = "Do you want to delete department %s" % department
    return render(request, 'delete_confirmation.html', {
        'message': msg,
    })
