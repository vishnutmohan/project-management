from itertools import chain

from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import *
from employees.models import *


class UserViewSet(viewsets.ViewSet):
    def list_all_users(self, request, *args, **kwargs):
        user_queryset = User.objects.all()
        serializer = UserSerializer(user_queryset, many=True)
        return Response(serializer.data)

    def list_single_user(self, request, pk, *args, **kwargs):
        user = get_object_or_404(User, id=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class EmployeeViewSet(viewsets.ViewSet):
    def list_all_employees(self, request, *args, **kwargs):
        employee_queryset = Employee.objects.all()
        serializer = EmployeeSerializer(employee_queryset, many=True)
        return Response(serializer.data)

    def list_single_employee(self, request, pk, *args, **kwargs):
        employee = get_object_or_404(Employee, id=pk)
        serializer = EmployeeSerializer(employee)
        return Response(serializer.data)
