from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from decorators import *
from .forms import *


# Create your views here.
@loggedin_or_error
def user_list(request):
    users = User.objects.exclude(is_superuser=True)
    return render(request, 'user_list.html', {
        'users': users
    })


def user_registration(request):
    form = AddUserForm(request.POST or None)

    if form.is_valid():
        user = form.save()
        if user is not None:
            messages.error(request,
                           "You need to complete the employee registration also. Otherwise you need to contact admin")
            return redirect('employee-register', user.id)
        messages.error(request, "Your registration failed")
        return redirect('user-list')
    return render(request, 'user_registration.html', {
        'form': form
    })


@loggedin_or_error
@user_or_error
def user_edit(request, pk=None):
    user = User.objects.get(id=pk)
    form = EditUserForm(request.POST or None)
    if form.is_valid():
        old_password = form.cleaned_data.get('old_password')
        if user.check_password(old_password):
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('new_password')
            if not user.check_password(password):
                user.set_password(password)
                messages.success(request, "Please login again due to password change")
            user.save()
            messages.success(request, "Your update completed successfully.")
            return redirect('user-list')
        messages.success(request, "Your update Failed.")
        return redirect('user-edit', user.id)
    return render(request, 'user_edit.html', {
        "form": form,
        "user": user
    })


@loggedin_or_error
@employee_or_error
@user_or_manager
def user_delete(request, pk=None):
    emp_manager = Employee.objects.get(user_id=request.user.id)
    user = User.objects.get(id=pk)
    employee = Employee.objects.get(user=user)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Yes':
            Employee.objects.filter(manager=employee).update(manager=emp_manager)
            user.delete()
            messages.error(request, "User deletion Success.")
            return redirect('user-list')
        messages.error(request, "User deletion failed.")
        return redirect('user-list')
    msg = "Do you want to delete user %s" % user
    return render(request, 'delete_confirmation.html', {
        'message': msg,
    })


def user_login(request):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('user-list')
        else:
            messages.error(request, 'invalid username or password')
            return render(request, 'user_login.html', {
                'form': form,
            })
    else:
        return render(request, 'user_login.html', {
            'form': form
        })


@loggedin_or_error
def user_logout(request):
    logout(request)
    return redirect('user-login')


def employee_registration(request, pk=None):
    form = AddEmployeeForm(request.POST or None)
    user = get_object_or_404(User, id=pk)
    departments = Department.objects.all()
    managers = Employee.objects.all()
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = user
        instance.save()
        messages.error(request, 'User registration completed. Please login')
        return redirect('user-login')
    context = {
        'user': user,
        'departments': departments,
        'managers': managers,
        'form': form
    }
    return render(request, 'employee_registration.html', context)


@loggedin_or_error
@employee_or_error
@employee_or_error
def employee_list(request):
    employees = Employee.objects.all()
    return render(request, 'employee_list.html', {
        'employees': employees
    })


@loggedin_or_error
@employee_or_error
@employee_or_manager_or_error
def employee_edit(request, pk=None):
    form = AddEmployeeForm(request.POST or None)
    employee = Employee.objects.get(id=pk)
    departments = Department.objects.all()
    managers = Employee.objects.all()

    if form.is_valid():
        employee.designation = form.cleaned_data.get('designation')
        employee.department = form.cleaned_data.get('department')
        employee.manager = form.cleaned_data.get('manager')
        employee.save()
        messages.success(request, "Your update completed successfully.")
        return redirect('employee-list')
    context = {
        'employee': employee,
        'departments': departments,
        'managers': managers,
        'form': form
    }
    return render(request, 'employee_edit.html', context)


@loggedin_or_error
@employee_or_error
@employee_or_manager
def employee_delete(request, pk):
    emp_manager = Employee.objects.get(user_id=request.user.id)
    employee = Employee.objects.get(id=pk)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Yes':
            Employee.objects.filter(manager=employee).update(manager=emp_manager)
            employee.delete()
            messages.error(request, "User deletion Success.")
            return redirect('employee-list')
        messages.error(request, "User deletion failed.")
        return redirect('employee-list')
    msg = "Do you want to delete employee %s" % employee
    return render(request, 'delete_confirmation.html', {
        'message': msg,
    })


@loggedin_or_error
def authorisation_delete_employee_error(request):
    error_message = 'You are not authorized to perform this action. Only Manager is authorized to do this operation'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('employee-list'),
        'error_message': error_message
    })


@loggedin_or_error
def authorisation_delete_user_error(request):
    error_message = 'You are not authorized to perform this action. Only Manager is authorized to do this operation'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('user-list'),
        'error_message': error_message
    })


@loggedin_or_error
def authorisation_edit_employee_error(request):
    error_message = 'You are not authorized to perform this action. Only the corresponding user or manager is authorized to do update details'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('employee-list'),
        'error_message': error_message
    })


@loggedin_or_error
def authorisation_edit_user_error(request):
    error_message = 'You are not authorized to perform this action. Only the corresponding user is authorized to do update details'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('user-list'),
        'error_message': error_message
    })


@loggedin_or_error
def authorisation_employee_error(request):
    error_message = 'You are not an employee to perform this action. Contact admin to register as employee and get authorized to do this operation'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('user-list'),
        'error_message': error_message
    })


@loggedin_or_error
def authorisation_manager_error(request):
    error_message = 'You are not the project manager to perform this action.'
    return render(request, 'authorisation_error.html', {
        'url_value': reverse('project-list'),
        'error_message': error_message
    })