from django.conf.urls import url

from .views import *
from .viewsets import *
from rest_framework.authtoken import views

urlpatterns = [
    url(r'^$', user_login, name='user-login'),
    url(r'^user-list/$', user_list, name='user-list'),
    url(r'^user-register/$', user_registration, name='user-register'),
    url(r'^user-logout/$', user_logout, name='user-logout'),
    url(r'^user/(?P<pk>\d+)/user-edit/$', user_edit, name='user-edit'),
    url(r'^user/(?P<pk>\d+)/user-delete/$', user_delete, name='user-delete'),
    url(r'^employees/$', employee_list, name='employee-list'),
    url(r'^employee/(?P<pk>\d+)/employee-register/$', employee_registration, name='employee-register'),
    url(r'^employee/(?P<pk>\d+)/employee-edit/$', employee_edit, name='employee-edit'),
    url(r'^employee/(?P<pk>\d+)/employee-delete/$', employee_delete, name='employee-delete'),
    url(r'^delete-user-authorisation-error/$', authorisation_delete_user_error, name='delete-user-authorisation-error'),
    url(r'^delete-employee-authorisation-error/$', authorisation_delete_employee_error,
        name='delete-employee-authorisation-error'),
    url(r'^edit-user-authorisation-error/$', authorisation_edit_user_error, name='edit-user-authorisation-error'),
    url(r'^edit-employee-authorisation-error/$', authorisation_edit_employee_error,
        name='edit-employee-authorisation-error'),
    url(r'^authorisation-employee-error/$', authorisation_employee_error, name='authorisation-employee-error'),
    url(r'^authorisation-manager-error/$', authorisation_manager_error, name='authorisation-manager-error'),
    url(r'get-all-users/$', UserViewSet.as_view({
        'get': 'list_all_users',
    })),
    url(r'get-user/(?P<pk>\d+)/$', UserViewSet.as_view({
        'get': 'list_single_user'
    })),
    url(r'get-all-employees/$', EmployeeViewSet.as_view({
        'get': 'list_all_employees',
    })),
    url(r'get-employee/(?P<pk>\d+)/$', EmployeeViewSet.as_view({
        'get': 'list_single_employee'
    })),
    url(r'^api-token-auth/', views.obtain_auth_token),

]
