from django import forms
from django.contrib.auth.models import User

from .models import *


class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255, widget=forms.PasswordInput)

    class Meta:
        fields = [
            'username',
            'password'
        ]


class AddUserForm(forms.ModelForm):
    first_name = forms.CharField(error_messages={
        'required': 'Enter first name',
    })
    last_name = forms.CharField(error_messages={
        'required': 'Enter last name',
    })
    email = forms.EmailField(error_messages={
        'required': 'Enter your email id',
    })
    username = forms.CharField(error_messages={
        'required': 'Enter your user name',
    })

    password = forms.CharField(widget=forms.PasswordInput, error_messages={
        'required': 'Please give password',})

    retype_password = forms.CharField(widget=forms.PasswordInput, error_messages={
        'required': 'Please give password',})

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if username != "" or username is not None:
            query = User.objects.filter(username=username)
            if query.count() > 0:
                raise forms.ValidationError('Username already exists.')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email != "" or email is not None:
            query = User.objects.filter(email=email)
            if query.count() > 0:
                raise forms.ValidationError('Email already exists.')
        return email

    def clean_retype_password(self):

        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('retype_password')

        if password1 != password2:
            raise forms.ValidationError('Both passwords must be same.')

    def save(self, commit=True):
        user = forms.ModelForm.save(self, commit=False)
        password = self.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        return user

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'username',
            'password',
            'retype_password',
        ]


class EditUserForm(forms.ModelForm):
    first_name = forms.CharField(error_messages={
        'required': 'Enter first name',
    })
    last_name = forms.CharField(error_messages={
        'required': 'Enter last name',
    })
    email = forms.EmailField(error_messages={
        'required': 'Enter your email id',
    })

    old_password = forms.CharField(widget=forms.PasswordInput, error_messages={
        'required': 'Please give password',})

    new_password = forms.CharField(widget=forms.PasswordInput, error_messages={
        'required': 'Please give password',})

    retype_new_password = forms.CharField(widget=forms.PasswordInput, error_messages={
        'required': 'Please give password',})

    def clean_retype_password(self):
        password1 = self.cleaned_data.get('new_password')
        password2 = self.cleaned_data.get('retype_new_password')

        if password1 != password2:
            raise forms.ValidationError('Both passwords must be same.')

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'old_password',
            'new_password',
            'retype_new_password',
        ]


class AddEmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = [
            'designation',
            'department',
            'manager',
        ]
