from rest_framework import serializers

from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'username',
        ]


class EmployeeSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    department = serializers.StringRelatedField()
    manager = serializers.StringRelatedField()

    class Meta:
        model = Employee
        fields = [
            'id',
            'user',
            'designation',
            'department',
            'manager',
        ]
