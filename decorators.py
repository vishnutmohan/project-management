from django.contrib.auth.models import User
from django.shortcuts import redirect

from employees.models import Employee
from projects.models import Project


def loggedin_or_home(fn):
    def _new(request, *args, **kwargs):
        if request.user.is_authenticated():
            return fn(request, *args, **kwargs)
        else:
            return redirect("user-login")

    return _new


def staff_or_home(fn):
    def _new(request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.is_staff:
            return fn(request, *args, **kwargs)
        else:
            return redirect("user-login")

    return _new


def loggedin_or_error(fn):
    def _new(request, *args, **kwargs):
        if request.user.is_authenticated():
            return fn(request, *args, **kwargs)
        else:
            return redirect("user-login")

    return _new


def employee_or_manager(fn):
    def _new(request, *args, **kwargs):
        deleted_user = kwargs.get('pk')
        current_user = str(request.user)
        employee = Employee.objects.get(id=deleted_user)
        manager = str(employee.manager)
        if manager == current_user:
            return fn(request, *args, **kwargs)
        else:
            return redirect('delete-employee-authorisation-error')

    return _new


def user_or_manager(fn):
    def _new(request, *args, **kwargs):
        deleted_user = kwargs.get('pk')
        current_user = str(request.user)
        if Employee.objects.filter(user_id=deleted_user).exists():
            employee = Employee.objects.get(user_id=deleted_user)
            manager = str(employee.manager)
            if manager == current_user:
                return fn(request, *args, **kwargs)
            else:
                return redirect('delete-user-authorisation-error')
        else:
            return redirect('delete-user-authorisation-error')

    return _new


def user_or_error(fn):
    def _new(request, *args, **kwargs):
        edit_user = kwargs.get('pk')
        current_user = str(request.user)
        user = User.objects.get(id=edit_user)
        username = str(user.username)
        if username == current_user:
            return fn(request, *args, **kwargs)
        else:
            return redirect('edit-user-authorisation-error')

    return _new


def employee_or_manager_or_error(fn):
    def _new(request, *args, **kwargs):
        edit_user = kwargs.get('pk')
        current_user = str(request.user)
        employee = Employee.objects.get(id=edit_user)
        manager = str(employee.manager)
        username = str(employee.user.username)
        if manager == current_user or username == current_user:
            return fn(request, *args, **kwargs)
        else:
            return redirect('edit-employee-authorisation-error')

    return _new


def employee_or_error(fn):
    def _new(request, *args, **kwargs):
        if Employee.objects.filter(user=request.user).exists():
            return fn(request, *args, **kwargs)
        else:
            return redirect('authorisation-employee-error')

    return _new


def manager_or_error(fn):
    def _new(request, *args, **kwargs):
        project_id = kwargs.get('pk')
        current_user = str(request.user)
        project = Project.objects.get(id=project_id)
        manager = str(project.manager)
        if manager == current_user:
            return fn(request, *args, **kwargs)
        else:
            return redirect('authorisation-manager-error')

    return _new
